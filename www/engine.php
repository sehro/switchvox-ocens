<?php
function performActions() {
    $conn = new PDO('sqlite:/opt/ocens/oncall.db');
    switch($_POST['act']) {
    case "deletable":
        $usrID = $_POST['id'];
        $eng = $conn->prepare("SELECT count(rank_id) FROM ranks WHERE rank_holder = $usrID");
        $eng->execute();
        echo $eng->fetch()[0];
        break;
    case "getUserName":
        $usrID = $_POST['id'];
        $eng = $conn->prepare("SELECT user_name FROM users WHERE user_id = $usrID");
        $eng->execute();
        $userName = $eng->fetch()[0];
        echo $userName;
        break;
    case "getUserPhone":
        $usrID = $_POST['id'];
        $eng = $conn->prepare("SELECT user_phone FROM users WHERE user_id = $usrID");
        $eng->execute();
        $userPhone = $eng->fetch()[0];
        echo $userPhone;
        break;
    case "getUserSMS":
        $usrID = $_POST['id'];
        $eng = $conn->prepare("SELECT user_sms_capable FROM users WHERE user_id = $usrID");
        $eng->execute();
        $userSMS = $eng->fetch()[0];
        echo $userSMS;
        break;
    case "addUser":
        $usrName = $_POST['name'];
        $usrPhone = $_POST['phone'];
        $usrSMS = $_POST['sms'];
        $usrSMSC = '0';
        // Handle $usrSMS bullshit
        if ($usrSMS) { $usrSMSC = '1'; }
        $stored_exc = null;
        try {
            $conn->query("INSERT INTO users (user_name,user_phone,user_sms_capable) VALUES ('$usrName', '$usrPhone', $usrSMSC);");
        }
        catch (Exception $e) {
            $stored_exc = $e;
            echo "Fail: ", $e->getMessage(), "\n";
        }
        if ($stored_exc) {
            throw($stored_exc);
        } else {
            echo "Changes saved!";
        }
        break;
    case "saveUser":
        $usrID = $_POST['id'];
        $usrName = $_POST['name'];
        $usrPhone = $_POST['phone'];
        $usrSMS = $_POST['sms'];
        $usrSMSC = '0';
        // Handle $usrSMS bullshit
        if ($usrSMS) { $usrSMSC = '1'; }
        $eng = $conn->prepare("UPDATE users SET user_name = ?, user_phone = ?, user_sms_capable = ? WHERE user_id = ?;");
        $stored_exc = null;
        try {
            $data = array($usrName,$usrPhone,$usrSMSC,$usrID);
            $eng->execute($data);
        }
        catch (Exception $e) {
            $stored_exc = $e;
            echo "Fail: ", $e->getMessage(), "\n";
        }
        if ($stored_exc) {
            throw($stored_exc);
        } else {
            echo "Changes saved!";
        }
        break;
    case "delUser":
        $usrID = $_POST['id'];
        $stored_exc = null;
        try {
            $conn->query("DELETE FROM users WHERE user_id = $usrID");
        } catch (Exception $e) {
            $stored_exc = $e;
            echo "Fail: ", $c->getMessage(), "\n";
        }
        if ($stored_exc) {
            throw($stored_exc);
        } else {
            echo "User removed!";
        }
        break;
    case "updateRank":
        $usrID = $_POST['user'];
        $rankID = $_POST['rank'];
        $stored_exc = null;
        $eng = $conn->prepare("UPDATE ranks SET rank_holder = ? WHERE rank_id = ?");
        try {
            $data = array($usrID,$rankID);
            $eng->execute($data);
        } catch (Exception $e) {
            $stored_exc = $e;
            echo "Fail: ", $e->getMessage(), "\n";
        }
        if ($stored_exc) {
            throw($stored_exc);
        } else { 
            echo "Rank saved";
        }
        break;
    default:
        echo "No action selected!";
}}
if (!isset($_POST['act'])) {
  echo "This module should not be called directly!";
} else {
  performActions();
}
?>
