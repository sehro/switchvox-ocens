$(function() {
    $(".userAdd")
        .button()
        .click(function() {
            $("#edID").val("0");
            $("#edName").val("");
            $("#edPhone").val("");
            $("#edSMS").prop('checked', false);
            $("#editor").dialog("open");
        })
    $(".userEdit")
        .button()
        .click(function() {
            usrID = $(this).val();
            $("#edSMS").prop('checked',false);
            $("#edID").val(usrID);
            $("#edName").val($.ajax({
                type: 'POST',
                url: "engine.php",
                data: {act: "getUserName", id: usrID},
                global: false,
                async: false,
                success: function(data) {
                    return data;
                }
            }).responseText);
            $("#edPhone").val($.ajax({
                type: 'POST',
                url: "engine.php",
                data: {act: "getUserPhone", id: usrID},
                global: false,
                async: false,
                success: function(data) {
                    return data;
                }
            }).responseText);
            var edSMSC = $.ajax({
                type: 'POST',
                url: "engine.php",
                data: {act: "getUserSMS", id: usrID},
                global: false,
                async: false,
                success: function(data) {
                    return data;
                }
            }).responseText;
            if (edSMSC == 1) {
                $("#edSMS").prop('checked',true);
            }
            $("#editor").dialog("open");
        })
    $(".userDel")
        .button()
        .click(function() {
            var isDeletable = $.ajax({
                type: 'POST',
                url: "engine.php",
                data: {act: "deletable", id: $(this).val()},
                context: document.body,
                global: false,
                async: false,
                success: function(data) {
                    return data;
                }
            }).responseText;
            if (isDeletable == '0') {
                var usrID = $(this).val()
                $("#query").html("Are you sure you want to delete user id " + usrID + "?").dialog({
                    modal: true,
                    buttons: {
                        No: function() { $(this).dialog("close"); },
                        Yes: function() {
                            var returnString = $.ajax({
                                type: 'POST',
                                url: "engine.php",
                                data: { act: "delUser", id: usrID },
                                context: document.body,
                                global: false,
                                async: false,
                                success: function(data) {
                                    return data;
                                },
                                failure: function() {
                                    return "Unable to reach engine!";
                                }
                            }).responseText;
                            $("#dialog").html(returnString).dialog({
                                modal: true,
                                buttons: { Ok: function() { location.reload(); } }
                            });
                            $(this).dialog("close");
                        }	
                    }
                })
            } else {
                $("#dialog").html("This user is assigned to a rank, and cannot be deleted at this time.").dialog({
                    modal: true,
                    buttons: { Ok: function() { $(this).dialog("close"); } }
                });
            }
        })
    $("#editor").dialog({
        autoOpen: false,
        modal: true,
        buttons: {
            Save: function() { 
                var usrID = $("#edID").val();
                var actionStr = "saveUser";
                if ( usrID == '0') { actionStr = "addUser"; }
                var returnString = $.ajax({
                    type: 'POST',
                    url: "engine.php",
                    data: {
                        act: actionStr,
                        id: $("#edID").val(),
                        name: $("#edName").val(),
                        phone: $("#edPhone").val(),
                        sms: $("#edSMS").prop("checked")
                    },
                    context: document.body,
                    global: false,
                    async: false,
                    success: function(data) {
                        return data;
                    },
                    failure: function() {
                        return "Unable to save changes!";
                    }
                }).responseText;
                $("#dialog").html(returnString).dialog({
                    modal: true,
                    buttons: { Ok: function() { location.reload(); } }
                });
                $(this).dialog("close");
            },
            Cancel:  function() { $(this).dialog("close"); }
        }
    });
    $("#rank1").selectmenu({
        change: function( event, data ) {
            var returnString = $.ajax({
                type: 'POST',
                url: "engine.php",
                data: { act: "updateRank", user: data.item.value, rank: '1' },
                context: document.body,
                global: false,
                async: false,
                success: function(data) {
                    return data;
                }
            }).responseText;
            $("#dialog").html(returnString).dialog({
                modal: true,
                buttons: { Ok: function() { $(this).dialog("close"); } }
            });
        }
    });
    $("#rank2").selectmenu({
        change: function( event, data ) {
            var returnString = $.ajax({
                type: 'POST',
                url: "engine.php",
                data: { act: "updateRank", user: data.item.value, rank: '2' },
                context: document.body,
                global: false,
                async: false,
                success: function(data) {
                    return data;
                }
            }).responseText;
            $("#dialog").html(returnString).dialog({
                modal: true,
                buttons: { Ok: function() { $(this).dialog("close"); } }
            });
        }
    });
    $("#rank3").selectmenu({
        change: function( event, data ) {
            var returnString = $.ajax({
                type: 'POST',
                url: "engine.php",
                data: { act: "updateRank", user: data.item.value, rank: '3' },
                context: document.body,
                global: false,
                async: false,
                success: function(data) {
                    return data;
                }
            }).responseText;
            $("#dialog").html(returnString).dialog({
                modal: true,
                buttons: { Ok: function() { $(this).dialog("close"); } }
            });
        }
    });
});
