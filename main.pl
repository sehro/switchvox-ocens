#!/bin/perl

use 5.010;
use warnings;
use DBI;
use Switchvox::API;
use File::Pid;
use File::Basename;
use IO::Socket::SSL;
use XML::Simple;
use LWP::Simple;
use Data::Dumper;
use URI::Escape::XS;
use Sys::Syslog;
use Cwd 'abs_path';

# Draw Header: Because reasons
#
my $headerText = <<'EOF';
      ___           ___           ___           ___           ___     
     /\  \         /\  \         /\  \         /\__\         /\  \    
    /::\  \       /::\  \       /::\  \       /::|  |       /::\  \   
   /:/\:\  \     /:/\:\  \     /:/\:\  \     /:|:|  |      /:/\ \  \  
  /:/  \:\  \   /:/  \:\  \   /::\~\:\  \   /:/|:|  |__   _\:\~\ \  \ 
 /:/__/ \:\__\ /:/__/ \:\__\ /:/\:\ \:\__\ /:/ |:| /\__\ /\ \:\ \ \__\
 \:\  \ /:/  / \:\  \  \/__/ \:\~\:\ \/__/ \/__|:|/:/  / \:\ \:\ \/__/
  \:\  /:/  /   \:\  \        \:\ \:\__\       |:/:/  /   \:\ \:\__\  
   \:\/:/  /     \:\  \        \:\ \/__/       |::/  /     \:\/:/  /  
    \::/  /       \:\__\        \:\__\         /:/  /       \::/  /   
     \/__/         \/__/         \/__/         \/__/         \/__/    


EOF
print $headerText;
sleep 2;

# Functions: We use these a lot
#
sub getSetting
{
    my($dbh, $settingName) = @_;

    my $settingValue = $dbh->selectall_arrayref('SELECT settingValue FROM config WHERE settingName = ?', undef, $settingName);

    return $settingValue->[0][0];
}

sub putSetting
{
    my($dbh, $settingName, $settingValue) = @_;
    my $sql = "UPDATE config SET settingValue = '".$settingValue."' WHERE settingName = '".$settingName."';";
    my $rs = $dbh->prepare($sql);
    $rs->execute() || &die_clean("Couldn't execute\n$sql\n".$dbh->errstr."\n");
}

sub getTarget
{
    my($dbh,$cur_rank) = @_;
    my $phoneNumber = $dbh->fetchrow_hashref('SELECT * FROM get_ranked_users WHERE rank_id = ?', undef, $cur_rank);

    return $phoneNumber->[0][0];
}

sub ringOut
{
    my($dbh, $PhoneTarget) = @_;
    my $api = new Switchvox::API(
        hostname => getSetting($dbh,"pbxHostName"),
        username => getSetting($dbh,"pbxUserName"),
        password => getSetting($dbh,"pbxPassword"),
        ssl_opts => { verify_hostname => 0, SSL_verify_mode => 0x00 }
    );
    my $phIVR = getSetting($dbh,"ivr_ext");
    my $phDAA = getSetting($dbh,"ivr_dialas");

    my $response = $api->api_request(
        method          => 'switchvox.call',
        parameters      =>{
            dial_first => $PhoneTarget,
            dial_second => $phIVR,
            dial_as_account_id => $phDAA,
            caller_id_name => 'OCENS',
            variables => "balance=300",
        }
    );

    if($response->{api_status} eq 'success')
    {
        syslog "info", "Call successfully made to $PhoneTarget.";
    }
    else
    {
        print "Encountered Errors:\n";
        foreach my $error ( @{$response->{api_errors}} )
        {
            print "-Code:$error->{code},Message:$error->{message}\n";
            syslog "err", "Code:$error->{code}, Message:$error->{message}";
        }
    }
}

sub smsOut
{
    my($dbh,$targetRank,$message) = @_;
    my $xml = new XML::Simple;
    my $targetUser = getTarget($dbh,$targetRank);
    my $smseUser = getSetting($dbh,"smseUser");
    my $smsePass = getSetting($dbh,"smsePass");
    my $smseNmbr = getSetting($dbh,"smseNmbr");
    if ($targetUser->{user_sms_capable} eq 1) {
        my $smsMessage = encodeURIComponent($message);
        my $prior = get( "https://smsout-api.vitelity.net/api.php?xml=yes&login=$smseUser&pass=$smsePass&cmd=sendsms&src=$smseNmbr&dst=$targetUser->{user_phone}&msg=$smsMessage" );
        die "Unable to parse API url!" unless defined $prior;
        my $doci = $xml->XMLin($prior);
        if ($doci->{response} eq 'ok') {
            print "SMS Sent to Rank $targetRank. ";
            syslog "info", "SMS sent to $targetUser->{user_phone}.";
        } else {
            print "Unable to send SMS to Rank $targetRank. ";
            syslog "err", "Unable to send SMS to $targetUser->{user_phone}.";
        }
    } else {
        print "No SMS number for Rank $targetRank. ";
        syslog "warning", "No sms number found for rank $targetRank.";
    }
}

sub getVMs
{
    my($dbh) = @_;
    my $count = 0;
    my $api = new Switchvox::API(
        hostname => getSetting($dbh,"pbxHostName"),
        username => getSetting($dbh,"pbxUserName"),
        password => getSetting($dbh,"pbxPassword"),
        ssl_opts => { verify_hostname => 0, SSL_verify_mode => 0x00 }
    );
    my $acctIVR = getSetting($dbh,"ivr_acct");

    my $response = $api->api_request(
        method => 'switchvox.users.voicemail.getList',
        parameters =>
        {
            account_id => $acctIVR,
            folder => 'INBOX',
        }
    );

    if($response->{api_status} eq 'success')
    {
        my @mailbox = $response->{api_result}{response}[0]{result}[0]{messages}[0]{message};
        #print Dumper $mailbox;
        foreach my $message (@mailbox){
            foreach my $ref (@$message){
                if ($ref->{read} eq 'No') { $count++; }
            }
        }
    }
    else
    {
        print "Encountered Errors:\n";
        foreach my $error ( @{$response->{api_errors}} )
        {
            print "-Code:$error->{code},Message:$error->{message}\n";
            syslog "err", "Code:$error->{code}, Message:$error->{message}";
        }
    }
    return $count;
}

# Syslog Facility: Save output to log system
#
openlog('OCENS', 'ndelay', LOG_LOCAL0);
syslog('info', 'Starting cycle');

# PID Wakelock: Am I already running
#
my $pidfile = File::Pid->new({
        file => '/var/run/ocens.pid',
    });

if (-e '/var/run/ocens.pid') {
    print "Failed!\n";
    syslog('err','PID file exists! Terminating early');
    die('Process already running; skip out');
} else {
    $pidfile->write;
}

# DB Interlink: Let's get us some data
#
my $dbpath = dirname(abs_path($0));
my $dbh = DBI->connect("dbi:SQLite:dbname=$dbpath/oncall.db","","") or die $DBI::errstr;
my $isEnabled = getSetting($dbh,"Enabled");
syslog('info','Connected to database');

# API Enable: Self Signed Certs Requires SSL Verification turned off
# This is because Vitelity (our default, and currently only, SMS engine, is crap)
# This may be removed in the future, because security is good
#
$ENV{PERL_LWP_SSL_VERIFY_HOSTNAME}=0;
IO::Socket::SSL::set_ctx_defaults(SSL_verify_mode => 0,);

say "ON CALL ESCALATION NOTIFICATION SYSTEM";

# VM Check: If there's voicemails, enable
my $vmCount = getVMs($dbh);
# Settings Get / Display
my $cur_rank = getSetting($dbh,"cur_rank");
my $max_rank = getSetting($dbh,"max_rank");
my $cd_escalate = getSetting($dbh,"cd_escalate");
my $cd_ringout = getSetting($dbh,"cd_ringout");
my $ring_inter = getSetting($dbh,"ring_inter");

say "\n===== SYSTEM STATUS =====\n";
# Debug Settings: Dump current settings to output
say "VM Count: $vmCount";
say "Current Rank: $cur_rank";
say "Max Rank: $max_rank";
say "Escalation Counter: $cd_escalate";
say "Ringout Counter: $cd_ringout";
say "Ring Interval: $ring_inter";
say "\n===== DAEMON OUTPUT =====\n";

if ($vmCount gt 0) {
    if ($isEnabled eq 'false') {
        putSetting($dbh,"cd_escalate",3);
        putSetting($dbh,"cd_ringout",1);
        putSetting($dbh,"cur_rank",1);
        smsOut($dbh,1,"OCENS: New messages in notification inbox. Starting Notification Cycle.");
        syslog('info','New voicemails detected. Starting cycle.');
    }
    $isEnabled = 'true';
    putSetting($dbh,'Enabled',$isEnabled);
} else {
    if ($isEnabled eq 'true') {
        my $cur_rank = getSetting($dbh,"cur_rank");
        for (my $iter = $cur_rank; $iter > 0; $iter -= 1) {
            smsOut($dbh,$iter,"OCENS: All messages acknowledged. Ending Notification Cycle.");
        }
        syslog('info','Voicemails cleared. Ending cycle.');
        putSetting($dbh,"cd_escalate",3);
        putSetting($dbh,"cd_ringout",0);
        putSetting($dbh,"cur_rank",1);
    } else {
        say "No voicemails found. Cycle disabled.";
    }
    $isEnabled = 'false';
    putSetting($dbh,'Enabled',$isEnabled);
}

# Enable check: See if we go now
if ($isEnabled eq 'true') {

# Main Logic Loop: It had to go somewhere
    #
    if ($cur_rank < 1)
    {
        say "Rank is invalid, clearing settings.";
        putSetting($dbh,"cd_escalate",3);
        putSetting($dbh,"cd_ringout",0);
        putSetting($dbh,"cur_rank",1);
        syslog('warning','Invalid Rank. Clearing settings and recycling.');
        exit 0;
    }

    if ($cd_ringout < 1)
    {
        if ($cd_escalate < 1)
        {
            if ($cur_rank < $max_rank) {
                # Perform escalation steps
                print "Escalating to Rank ".++$cur_rank.". ";
                $cd_escalate = 2;
                for (my $iter = ($cur_rank - 1); $iter > 0; $iter -= 1) {
                    smsOut($dbh,$iter,"OCENS: Escalating to Rank $cur_rank");
                }
                syslog "info","Escalating to rank $cur_rank";
                putSetting($dbh,"cur_rank",$cur_rank);
                putSetting($dbh,"cd_escalate",$cd_escalate);
            } else {
                # Reset to rank 1
                print "Recycling to Rank 1. ";
                $cd_escalate = 2;
                for (my $iter = 3; $iter > 0; $iter -= 1) {
                    smsOut($dbh,$iter,"OCENS: Recycling to Rank 1");
                }
                syslog "info", "Recycling to rank 1";
                $cur_rank = 1;
                putSetting($dbh,"cur_rank",$cur_rank);
                putSetting($dbh,"cd_escalate",$cd_escalate);
            }
        } else {
            print "Escalating in ".$cd_escalate--." ringouts. ";
            putSetting($dbh,"cd_escalate",$cd_escalate);
        }

        # Perform ringout steps
        my $targetUser = getTarget($dbh,$cur_rank);
        print "Connecting IVR to $targetUser->{user_name} at $targetUser->{user_phone}.\n";
        syslog "info", "Ringing out to target $targetUser->{user_name} at $targetUser->{user_phone}.";
        ringOut($dbh,$targetUser->{user_phone});
        $cd_ringout = $ring_inter;
        putSetting($dbh,"cd_ringout",$cd_ringout);
    } else {
        print "Waiting for Ringout Timer.\n";
        $cd_ringout--;
        putSetting($dbh,"cd_ringout",$cd_ringout);
    }

}

# Cleanup: Always perform the following
#
END{
    $dbh->disconnect();
    $pidfile->remove;
    syslog "info", "Exiting.";
    closelog();
}
