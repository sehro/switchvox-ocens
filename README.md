Switchvox OCENS Programme

The ON CALL EMERGENCY NOTIFICATION SYSTEM

It connects to the PBX, monitors a voicemail box for entries, and handles them in a predetermined manner.

Requires the following Perl modules to be installed
*   DBI
*   Switchvox::API
*   File::Pid
*   File::Basename
*   IO::Socket::SSL
*   XML::Simple
*   LWP::Simple
*   Data::Dumper
*   URI::Escape::XS
*   Sys::Syslog
*   Cwd

The www/ directory contains the admin panel, which is currently only mostly functional. The ability to set SMS Engine parameters is not yet enabled. Also, the admin panel has no built in security, and thus relies on the underlying HTTP/PHP engine to do it's job.

Upcoming changes:
*   Replace PHP-based admin panel with something not terrible.
*   Make admin panel use authentication without special settings changes on underlying web server.
*   Support more SMS Engines than vitelity; either freeform API entry, or several different engines. Likely, whichever is easier to implement.
*   Make it look prettier.
