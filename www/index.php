<?php
$conn = new PDO('sqlite:/opt/ocens/oncall.db');
$content = "";
$tabs = "";
$debug = "";
function addLine($addstr) {
    global $content;
    $content .= $addstr;
    $content .= "\n";
}
function addDebugLine($addstr) {
    global $debug;
    $debug .= "<br />";
    $debug .= $addstr;
    $debug .= "\n";
}
function setTab($tabID) {
    global $tabs;
    switch($tabID) {
    case 1:
        $tabs .= "<div class='tab'><a href='?'><span class='tablink'>Home</span></a></div>\n";
        $tabs .= "<div class='tab' id='activeTab'>Users</div>\n";
        $tabs .= "<div class='tab'><a href='?t=ranks'><span class='tablink'>Ranks</span><</a></div>\n";
        $tabs .= "<div class='tab'><a href='?t=settings'><span class='tablink'>Settings</span><</a></div>\n";
        break;
    case 2:
        $tabs .= "<div class='tab'><a href='?'><span class='tablink'>Home</span></a></div>\n";
        $tabs .= "<div class='tab'><a href='?t=users'><span class='tablink'>Users</span><</a></div>\n";
        $tabs .= "<div class='tab' id='activeTab'>Ranks</div>\n";
        $tabs .= "<div class='tab'><a href='?t=settings'><span class='tablink'>Settings</span><</a></div>\n";
        break;
    case 3:
        $tabs .= "<div class='tab'><a href='?'><span class='tablink'>Home</span></a></div>\n";
        $tabs .= "<div class='tab'><a href='?t=users'><span class='tablink'>Users</span><</a></div>\n";
        $tabs .= "<div class='tab'><a href='?t=ranks'><span class='tablink'>Ranks</span><</a></div>\n";
        $tabs .= "<div class='tab' id='activeTab'>Settings</div>\n";
        break;
    default:
        $tabs .= "<div class='tab' id='activeTab'>Home</div>\n";
        $tabs .= "<div class='tab'><a href='?t=users'><span class='tablink'>Users</span><</a></div>\n";
        $tabs .= "<div class='tab'><a href='?t=ranks'><span class='tablink'>Ranks</span><</a></div>\n";
        $tabs .= "<div class='tab'><a href='?t=settings'><span class='tablink'>Settings</span><</a></div>\n";
    }
}
if (!isset($_GET['t'])) {
    $_GET['t'] = '0';
}
switch($_GET['t']) {
case "users":
    setTab(1);
    addLine("<p>You can adjust users here.</p>");
    addLine("<table>");
    addLine("<tr><th>User</th><th width='115px' style='text-align: center;'>Actions</th></tr>");
    $eng = $conn->prepare("SELECT * FROM users");
    $eng->execute();
    foreach ($eng->fetchAll() as $uRow) {
        addLine("<tr><td>$uRow[1]</td><td><button class='userEdit' value='$uRow[0]'>Edit</button> <button class='userDel' value='$uRow[0]'>Delete</button></td></tr>");
    }
    addLine("<tr><td colspan='2'><br /><button class='userAdd'>Add a user</button></td></tr>");
    addLine("</table>");
    break;
case "ranks":
    setTab(2);
    addLine("<p>Select the users you wish to be assigned to each rank.</p>");
    // Rank 1
    addLine("<div class='rank'>");
    addLine("<h3>Tier 1</h3>");
    addLine("<select class='rankmenu' id='rank1'>");
    $eng = $conn->prepare("SELECT rank_holder FROM ranks WHERE rank_id = 1");
    $eng->execute();
    $rank1 = $eng->fetch()[0];
    $eng = $conn->prepare("SELECT user_id,user_name FROM users ORDER BY user_name");
    $eng->execute();
    foreach ($eng->fetchAll() as $uRow) {
        if($uRow[0] == $rank1) {
            addLine("<option value=$uRow[0] selected>$uRow[1]</option>");
        } else {
            addLine("<option value=$uRow[0]>$uRow[1]</option>");
        }
    }
    addLine("</select>");
    addLine("</div>");
    // Rank 2
    addLine("<div class='rank'>");
    addLine("<h3>Tier 2</h3>");
    addLine("<select class='rankmenu' id='rank2'>");
    $eng = $conn->prepare("SELECT rank_holder FROM ranks WHERE rank_id = 2");
    $eng->execute();
    $rank2 = $eng->fetch()[0];
    $eng = $conn->prepare("SELECT user_id,user_name FROM users ORDER BY user_name");
    $eng->execute();
    foreach ($eng->fetchAll() as $uRow) {
        if($uRow[0] == $rank2) {
            addLine("<option value=$uRow[0] selected>$uRow[1]</option>");
        } else {
            addLine("<option value=$uRow[0]>$uRow[1]</option>");
        }
    }
    addLine("</select>");
    addLine("</div>");
    // Rank 3
    addLine("<div class='rank'>");
    addLine("<h3>Tier 3</h3>");
    addLine("<select class='rankmenu' id='rank3'>");
    $eng = $conn->prepare("SELECT rank_holder FROM ranks WHERE rank_id = 3");
    $eng->execute();
    $rank3 = $eng->fetch()[0];
    $eng = $conn->prepare("SELECT user_id,user_name FROM users ORDER BY user_name");
    $eng->execute();
    foreach ($eng->fetchAll() as $uRow) {
        if($uRow[0] == $rank3) {
            addLine("<option value=$uRow[0] selected>$uRow[1]</option>");
        } else {
            addLine("<option value=$uRow[0]>$uRow[1]</option>");
        }
    }
    addLine("</select>");
    addLine("</div>");
    break;
case "settings":
    setTab(3);
    addLine("Settings are displayed below.");
    addLine("<ul>");
    $eng = $conn->prepare("SELECT * FROM config where settingName NOT IN ('pbxPassword','ivr_acct')");
    $eng->execute();
    foreach ($eng->fetchAll() as $sRow) {
        addLine("<li>$sRow[1]: $sRow[2]</li>");
    }
    addLine("</ul>");
    break;
default:
    setTab(0);
    addLine("<h3>Welcome to the control panel!</h3>");
    addLine("<p>This is where changes can be made to the On Call Emergency Notification System.");
    addLine("Please note the following caveats:");
    addLine("<ol><li>Users assigned to a rank cannot be deleted.</li>");
    addLine("<li>Since most of them are not worth changing, settings are read-only at this time.</li>");
    addLine("<li>All ranks must have a user assigned at all times. There is not currently a bypass function for empty ranks.</li>");
    addLine("<li>This app is poorly styled, because I spent most of the time rewriting everything to work better.</li>");
    addLine("</ol>");
    addLine("If you have any questions, please contact D2R SS&amp;A.");
}
?>
<!DOCTYPE html>
<html>
<head>
<title>OCENS Control Panel</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
<script src="script.js"></script>
<link rel="stylesheet" href="style.css" type="text/css" />
</head>
<body>
<h2>OCENS Control Panel</h2>
<div id="dialog" title="OCENS Message">
</div>
<div id="query" title="OCENS Prompt">
</div>
<div id="editor" title="OCENS Editor">
<table style="width: 285px !important;">
    <tr><td><b>Name:</b></td><td><input type="text" id="edName"></input></td></tr>
    <tr><td><b>Phone:</b></td><td><input type="text" id="edPhone"></input></td></tr>
    <tr><td><b>SMS:</b></td><td><input type="checkbox" id="edSMS" /></td></tr>
</table>
<input type="hidden" id="edID" value="0">
</div>
<div id="tabs">
<?php print $tabs; ?>
</div>
<div id="content">
<?php print $content; ?>
</div>
<?php
// Debug Console
if($debug != "") {
    print "<div id='debug'>\n";
    print "<b>===== DEBUG LOG =====</b>\n";
    print $debug;
    print "</div>\n";
} else {
    print "<div id='debug' style='display: none;'></div>\n";
}

?>
</body>
</html>
